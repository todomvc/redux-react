import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as TodoActions from "./Actions";
import { getVisibleTodos } from "./Selectors";
import TodoItem from "./TodoItem";
import React from "react";

const TodoListView = ({
  filteredTodos,
  actions
}: {
  filteredTodos: Array<{ id: number; completed: boolean; text: string }>;
  actions: {
    editTodo: (id: number, text: string) => void;
    deleteTodo: (id: number) => void;
    completeTodo: (id: number) => void;
  };
}) => (
  <ul className="todo-list">
    {filteredTodos.map(todo => (
      <TodoItem key={todo.id} todo={todo} {...actions} />
    ))}
  </ul>
);

const mapStateToProps = (state: {
  todos: Array<{ id: number; completed: boolean; text: string }>;
  visibilityFilter: string;
}) => ({
  filteredTodos: getVisibleTodos(state)
});

const mapDispatchToProps = (dispatch: any) => ({
  actions: bindActionCreators(TodoActions, dispatch)
});

const TodoList = connect(
  mapStateToProps,
  mapDispatchToProps
)(TodoListView);

export default TodoList;
