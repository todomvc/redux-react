import { connect } from 'react-redux'
import { addTodo } from './Actions'
import TodoEntry from "./TodoEntry";
import React from "react";

const TodoHeader = ({ addTodo }: { addTodo: (v: string) => void }) => (
    <header className="header">
        <h1>todos</h1>
        <TodoEntry
            newTodo
            onSave={text => {
                if (text.length !== 0) {
                    addTodo(text);
                }
            }}
            placeholder="What needs to be done?"
        />
    </header>
);

export default connect(null, { addTodo })(TodoHeader)
