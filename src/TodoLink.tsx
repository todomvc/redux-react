import { connect } from "react-redux";
import { setVisibilityFilter } from "./Actions";
import React from "react";
import classnames from "classnames";

const Link = ({
  active,
  children,
  setFilter
}: {
  active: boolean;
  children: React.ReactNode;
  setFilter: () => void;
}) => (
  <a
    className={classnames({ selected: active })}
    style={{ cursor: "pointer" }}
    onClick={() => setFilter()}
  >
    {children}
  </a>
);

const mapStateToProps = (
  state: { visibilityFilter: string },
  ownProps: { filter: string }
) => ({
  active: ownProps.filter === state.visibilityFilter
});

const mapDispatchToProps = (dispatch: any, ownProps: { filter: string }) => ({
  setFilter: () => {
    dispatch(setVisibilityFilter(ownProps.filter));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Link);
