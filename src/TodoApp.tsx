import React from 'react'
import Header from './TodoHeader'
import MainSection from './TodoOverview'

const TodoApp = () => (
  <div>
    <Header />
    <MainSection />
  </div>
)

export default TodoApp
