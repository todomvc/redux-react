import React from "react";
import { render } from "react-dom";
import { createStore } from "redux";
import { Provider } from "react-redux";
import TodoApp from "./TodoApp";
import reducer from "./RootReducer";
import "todomvc-app-css/index.css";

const store = createStore(reducer);

render(
  <Provider store={store}>
    <TodoApp />
  </Provider>,
  document.getElementById("root")
);
