import React, { Component } from "react";
import classnames from "classnames";

interface ITodoTextInputProps {
  newTodo?: boolean;
  editing?: boolean;
  placeholder?: string;
  text?: string;
  onSave: (text: string) => void;
}

interface ITodoTextInputState {
  text: string;
}

export default class TodoEntry extends Component<
  ITodoTextInputProps,
  ITodoTextInputState
> {
  state = {
    text: this.props.text || ""
  };

  handleSubmit = (e: React.KeyboardEvent<HTMLInputElement>) => {
    const text = (e.target as HTMLInputElement).value.trim();
    if (e.which === 13) {
      this.props.onSave(text);
      if (this.props.newTodo) {
        this.setState({ text: "" });
      }
    }
  };

  handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ text: e.target.value });
  };

  handleBlur = (e: React.FocusEvent<HTMLInputElement>) => {
    if (!this.props.newTodo) {
      this.props.onSave(e.target.value);
    }
  };

  render() {
    return (
      <input
        className={classnames({
          edit: this.props.editing,
          "new-todo": this.props.newTodo
        })}
        type="text"
        placeholder={this.props.placeholder}
        autoFocus={true}
        value={this.state.text}
        onBlur={this.handleBlur}
        onChange={this.handleChange}
        onKeyDown={this.handleSubmit}
      />
    );
  }
}
