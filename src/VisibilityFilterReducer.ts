import { SET_VISIBILITY_FILTER } from "./ActionTypes";
import { SHOW_ALL } from "./TodoFilters";

const visibilityFilterReducer = (
  state = SHOW_ALL,
  action: { type: string; filter: string }
) => {
  switch (action.type) {
    case SET_VISIBILITY_FILTER:
      return action.filter;
    default:
      return state;
  }
};

export default visibilityFilterReducer;
