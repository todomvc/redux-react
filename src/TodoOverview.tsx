import { connect } from "react-redux";
import * as TodoActions from "./Actions";
import { bindActionCreators } from "redux";
import { getCompletedTodoCount } from "./Selectors";
import TodoList from "./TodoList";
import TodoFooter from "./TodoFooter";
import React from "react";

const MainSection = ({
  todosCount,
  completedCount,
  actions
}: {
  todosCount: number;
  completedCount: number;
  actions: {
    completeAllTodos: () => void;
    clearCompleted: () => void;
  };
}) => (
  <section className="main">
    {!!todosCount && (
      <span>
        <input
          className="toggle-all"
          type="checkbox"
          checked={completedCount === todosCount}
          readOnly
        />
        <label onClick={actions.completeAllTodos} />
      </span>
    )}
    <TodoList />
    {!!todosCount && (
      <TodoFooter
        completedCount={completedCount}
        activeCount={todosCount - completedCount}
        onClearCompleted={actions.clearCompleted}
      />
    )}
  </section>
);

const mapStateToProps = (state: {
  todos: Array<{ id: number; completed: boolean; text: string }>;
}) => ({
  todosCount: state.todos.length,
  completedCount: getCompletedTodoCount(state)
});

const mapDispatchToProps = (dispatch: any) => ({
  actions: bindActionCreators(TodoActions, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MainSection);
