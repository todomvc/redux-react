import { combineReducers } from 'redux'
import todosReducer from './TodosReducer'
import visibilityFilterReducer from './VisibilityFilterReducer'

const rootReducer = combineReducers({
  todos: todosReducer,
  visibilityFilter: visibilityFilterReducer
})

export default rootReducer
